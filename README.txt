TAXONOMY REFERENCE SHIELD

This module allows you to protect taxonomy terms from being
deleted if they are referenced from any entity reference
field.

Here is how it works:

1. Download and install the module as usual.

2. As the super administrator (or a user with the permission
'Administer the Taxonomy Reference Shield Module'), go to
'/admin/config/system/taxonomy-reference-shield' in order to
configure the module.

Set the vocabularies whose terms should be protected by this
module and save the form.

3. Those terms, should now be protected from deleting.
